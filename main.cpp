#include <iostream>
#include "MyString.h"
#include "LMystring.h"

using namespace std;

int main()
{
    cout << "Hello world!" << endl;

    //main de MyString
    MyString s1("iut"), s2("info");
    s1.affiche(); cout << s2 << endl;
    s1.setStr("test"); cout << s1 << endl;
    s1=s2; cout << s1 << endl;
    s1+=s2; cout << s1 << endl;
    s1-'i'; s1.suppr('n'); s1.dedouble('f'); cout << "s1 : " << s1 << endl;
    s1 = s1+s2; cout << s1 << endl;
    /**
    * programmer Get/Set (selon attribut)
    * methode concat
    **/

    //main de LMystring (fini)
    /*
    LMystring str1("iut"), str2(4,'c'), str3(str2);
    str1.affiche(); str2.affiche(); str3.affiche();
    str3 = str1; str3.affiche();
    str1.suppr('i'); str1.affiche();
    str2.Mytoupper(); str2.affiche();
    */
    exit(0);
}
