#ifndef MYSTRING_H
#define MYSTRING_H
#include<iostream>

using namespace std;

class MyString
{
    public:
        MyString();
        MyString(const char*);
        MyString(char, int);
        MyString(const MyString &); //ctor de recopie
        MyString&operator=(const MyString&); //surcharge =
        MyString&operator+=(const MyString&); //surcharge +=
        MyString operator+(const MyString &); //surcharge +
        MyString&operator-(const char); //surcharge -
        ~MyString();
        void affiche(); //= setStr
        void suppr(char);
        void dedouble(char);
        void setStr(const char*); //permet de remplir tab
        void getStats();
        int length(); //longeur de tab sans le \0
        void concat(const MyString&);

    protected:

    private:
        char *tab;
        int *stats;
        int nbl,spe;
        void majstats();
    friend ostream& operator<<(ostream&, const MyString&);
};

#endif // MYSTRING_H
