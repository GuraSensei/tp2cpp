#ifndef LMYSTRING_H
#define LMYSTRING_H

typedef struct liste
{
    char info;
    struct liste*svt;
}List, *plist;

class LMystring
{
    public:
        LMystring();
        LMystring(const char*);
        LMystring(int, char);
        ~LMystring();
        LMystring(const LMystring&);
        LMystring&operator=(const LMystring&);
        void affiche();
        void suppr(char);
        void Mytoupper();

    protected:

    private:
        plist tete;
        int n,spe, *stat;
        void maj();
};

#endif // LMYSTRING_H
