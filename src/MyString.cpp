#include "MyString.h"
#include <string.h>
#include <iostream>


using namespace std;

//prgm chaine (tp2 C)
char* supprime(char *source, char car)
{
char *cible=NULL;
int taille, cpt, i, j;

    for(i=0, cpt = 0; source[i]!='\0';i++) //etape 1
        if (source[i]== car) cpt++;

    if(cpt<i)
    {
        taille = i - cpt+1; //etape 2
        cible=(char*) malloc(taille*sizeof(char)); //etape 3
        for(i=0,j=0;source[i]!='\0'; i++)
            {
                if(source[i]!= car) //etape 4
                {cible[j++]=source[i];}
            }
        cible[j]='\0';
    }
    free(source);//etape 5
    source = cible;
    return cible; //etape 6
}

char* deDouble(char* source, char car)
/**
 * Return NULL if the function failed
 * 2 way of use : strDedouble = dedouble(source, car); or dedouble(source, car);
 * At the end strDedouble = source; because will be free() in the process
 */
{
    int cpt, taille, i, j;
    char* cible = NULL;

    for(i=0, cpt = 0; source[i]!='\0';i++) //etape 1
        if (source[i]== car) cpt++;
    if(cpt<= i)
    {
        taille = i+cpt+1;
        cible = (char*)malloc(taille*sizeof(char));
        for(i=0, j=0; i<taille; i++)
        {
            cible[i] = source[j++];
            if(source[i] == car)
            {
                i++;
                cible[i] = car;
            }
        }
        free(source);
        source = cible;
        return cible;
    }
    return NULL;
}
//fin prgm chaine

//Constructeurs
MyString::MyString()
{
    tab = NULL;
    nbl = 0; spe=0;
    stats = new int [26];
    for (int i=0; i<26; i++) stats[i] =0;
}

MyString::MyString(const char* source)
{   stats = NULL;
    tab = new char [strlen(source)+1];
    strcpy(tab, source);
    majstats();
}

MyString::MyString(char car, int nb)
{
    tab = new char [nb+1];
    stats = NULL;
    for(int  i=0; i<nb; i++) tab[i]=car;
    tab[nb] = '\0';
    majstats();
}

MyString::MyString(const MyString &str)
{
    nbl = str.nbl;
    spe = str.spe;
    tab = new char [nbl+1];
    strcpy(tab, str.tab);
    stats = new int [26];
    for(int i=0; i<26; i++)
        stats[i] = str.stats[i];
}

MyString::~MyString()
{
    if(stats!=NULL) delete stats;
    if(tab!=NULL) delete tab;
}

void MyString::majstats()
{
    int i;
    char car;

    if (stats==NULL) stats = new int[26];

    for(i=0; i<26; i++) stats[i]=0;
    spe=0;
    for(i=0; tab[i]!='\0'; i++)
    {
        car= tolower(tab[i]);
        if ((car >= 'a') && (car <='z'))
            stats[car-'a'] += 1;
        else spe++;
    }
    nbl=i;
}

void MyString::affiche()
{
    for(int i=0; tab[i]!='\0'; i++) printf("%c", tab[i]);
    printf("\n");
}

void MyString::suppr(char c)
{
   tab = supprime(tab, c);
   majstats();
}

void MyString::dedouble(char c)
{
    tab = deDouble(tab, c);
    majstats();
}

void MyString::setStr(const char *source)
{
    int i;
    for(i=0; source[i]!='\0'; i++) tab[i] = source[i];
    tab[i] = '\0';
    majstats();
}

//getStr = affiche();

void MyString::getStats()
{
    cout << "spe = " << spe << endl;
    for(int i=0; i<26; i++)
        if(stats[i]!=0) cout << stats[i] << " caractere " << (char)(i+'a') << endl;
}

int MyString::length()
{
    return nbl - 1;
}

void MyString::concat(const MyString& str)
{
    strcat(tab, str.tab);
}

MyString& MyString::operator=(const MyString &md)
{
    if(this!=&md)
    {
        delete tab; //on laisse stats
        nbl=md.nbl; spe = md.spe;
        tab = new char [nbl+1];
        strcpy(tab,md.tab);
        for(int i=0; i<26; i++) stats[i] = md.stats[i];
    }
    return (*this);
}


MyString& MyString::operator+=(const MyString &s1)
{
    strcat(tab, s1.tab);
    majstats();
    return (*this);
}

MyString MyString::operator+(const MyString &s2)
{
    char* tmp;
    tmp= (char*)malloc(1*sizeof(char));
    tmp[0] = '\0';
    strcat(tmp, tab);
    strcat(tmp, s2.tab);
    MyString res(tmp);
    free(tmp);
    majstats();
    return res;
}


MyString&MyString::operator-(const char car)
{
    tab = supprime(tab, car);
    majstats();
    return (*this);
}



// surcharge <<
ostream& operator<<(ostream& os, const MyString& obj)
{
    os << "chaine= " << obj.tab << endl;
    os << "n= " << obj.nbl << endl;
    os << "spe= " << obj.spe << endl;
    for(int i=0; i<26; i++) if(obj.stats[i]!=0)
        os << obj.stats[i] << " caractere " << (char)(i+'a') << endl;
    return os;
}
